'use strict';

angular.module("App")
	.constant("Forms", {
		"1a": {
			name: "première personne singulier",
			pronoun: {
				default: 'je',
				startsWithAEIO: {
					name: 'j\'',
				},
			},
		},
		"1b": {
			name: "première personne pluriel",
			pronoun: {
				default: 'nous',
			},
		},
		"2a": {
			name: "deuxieme personne singulier",
			pronoun: {
				default: 'tu',
			},
		},
		"2b": {
			name: "deuxieme personne pluriel",
			pronoun: {
				default: 'vous',
			},
		},
		"3a": {
			name: 'troisieme personne singulier',
			pronoun: {
				default: [
					'il',
					'elle',
					'on',
				],
			},
		},
		"3b": {
			name: 'troisieme personne pluriel',
			pronoun: {
				default: [
					'ils',
					'elles',
				],
			},
		}
	});
