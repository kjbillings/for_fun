'use strict';

angular.module("App")
	.constant("Verbs", [
		{
			infinitif: 'Penser',
			tenses: [
				{
					tense: 'passé composé',
					answers: {
						"1a": 'ai pensé',
						"1b": 'avons pensé',
						"2a": 'as pensé',
						"2b": 'avez pensé',
						"3a": 'a pensé',
						"3b": 'ont pensé',
					},
				},
				{
					tense: 'imparfait',
					answers: {
						"1a": 'pensais',
						"1b": 'pensions',
						"2a": 'pensais',
						"2b": 'pensiez',
						"3a": 'pensait',
						"3b": 'pensaient',
					},
				},
				{
					tense: 'présent',
					answers: {
						"1a": 'pense',
						"1b": 'pensons',
						"2a": 'penses',
						"2b": 'pensez',
						"3a": 'pense',
						"3b": 'pensent',
					},
				},
				{
					tense: 'futur',
					answers: {
						"1a": 'penserai',
						"1b": 'penserons',
						"2a": 'penseras',
						"2b": 'penserez',
						"3a": 'pensera',
						"3b": 'penseront',
					},
				},
				{
					tense: 'conditionnel',
					answers: {
						"1a": 'penserais',
						"1b": 'penserions',
						"2a": 'penserais',
						"2b": 'penseriez',
						"3a": 'penserait',
						"3b": 'penseraient',
					},
				},
			],
		},
	]);
