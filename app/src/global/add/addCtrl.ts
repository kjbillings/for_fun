declare var angular: any;
angular.module('App')
    .controller('AddCtrl', ['$scope', '$http', 'Forms',
		function ($scope, $http, forms) {
            var getStructure = function () {
                $http.get("http://localhost:3601/api/structure").then(function(response){
                    $scope.newVerb = angular.copy(response.data);
                });
            };
            getStructure();
			$scope.forms = forms;
	        $scope.menuOpen = false;
			$scope.toggleMenu = function () {
				$scope.menuOpen = !$scope.menuOpen;
			};
            $scope.save = function (verb) {
                $http.post("http://localhost:3601/api/verbs/add", {name: verb.infinitif, verb: verb})
                .then(function(response){
                    if(angular.isDefined(response.data.status) && response.data.status !== 200){
                        console.error(response.data);
                    }
                });
            };
	    }
	]);
