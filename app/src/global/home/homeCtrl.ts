declare var angular: any;
declare var $: any;
angular.module('App')
    .controller('HomeCtrl', ['$scope', '$http', 'Forms', function ($scope, $http, forms) {
		var verbs = [],
        getRandomInt = function (min, max) {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		getRandomProperty = function (obj) {
			var arr = Object.keys(obj);
			return obj[ arr[ getRandomInt( 0, arr.length-1 ) ] ];
		},
		getRandomVerb = function () {
			return verbs[ getRandomInt(0, verbs.length - 1) ];
		},
		getPronoun = function (pronoun, answer) {
			var p = pronoun.default,
			fl = answer.slice(0, 1).toLowerCase();
			if(angular.isDefined(pronoun.startsWithAEIO) && (fl === 'a' || fl === 'e' || fl === 'i' || fl === 'o')){
				return pronoun.startsWithAEIO.name;
			}
			if(angular.isArray(p)){
				return p[ getRandomInt(0, p.length-1) ];
			}
			return p;
		},
        getRandomTense = function (tenses) {
            return tenses[ getRandomInt(0, 4) ];
        },
        getRandomTense = function (tenses) {
            return tenses[ getRandomInt(0, 4) ];
        },
		answerAccepted = function () {
            $scope.answerGood = true;
            $scope.answerBad = false;
		},
		answerRejected = function () {
            $scope.answerGood = false;
            $scope.answerBad = true;
		},
        resetAnswerColor = function () {
            $scope.answerGood = false;
            $scope.answerBad = false;
        },
        resetAnswer = function () {
            $scope.answer = '';
        },
		getRandomAnswerKey = function (answers) {
			var keys = Object.keys(answers);
			return keys[ getRandomInt(0, keys.length-1 )];
		},
        commands = function(answer){
            var x = true;
            switch(answer.toLowerCase()){
                case 'show history':
                    $scope.showingHistory = true;
                break;
                case 'hide history':
                    $scope.showingHistory = false;
                break;
                case 'show count':
                    $scope.showingCount = true;
                break;
                case 'hide count':
                    $scope.showingCount = false;
                break;
                case 'show skip':
                    $scope.showingSkip = true;
                break;
                case 'hide skip':
                    $scope.showingSkip = false;
                break;
                case 'show cheats':
                    $scope.cheatMode = true;
                break;
                case 'hide cheats':
                    $scope.cheatMode = false;
                break;
                case 'i solemnly swear i am up to no good':
                    $scope.cheatMode = true;
                break;
    			case 'mischief managed':
                    $scope.cheatMode = false;
                break;
                default: x = false;
    		}
            if(x){
                $scope.resetCard();
            }
            return x;
        },
        answerWorks = function (answer) {
            return answer === $scope.card.answer;
        },
        addHistory = function () {
            $scope.history.push($scope.card);
        },
        getVerbs = function (callback) {
            $http.get("http://localhost:3601/api/verbs")
            .then(function(response){
                verbs = response.data.verbs;
                callback();
            });
        };
        $scope.cheatMode = false;
        $scope.resetCard = function () {
            resetAnswer();
            $('input[name="answer"]').val('').focus();
            resetAnswerColor();
        };
        $scope.getCard = function () {
            var verb = getRandomVerb(), tenses, tense, answerKey, answer;
            tenses = Object.keys(verb.tenses);
            tense = verb.tenses[tenses[getRandomInt(0, tenses.length - 1)]];
            answerKey = getRandomAnswerKey(tense.answers);
            answer = tense.answers[answerKey] || "answer unknown";
            $scope.resetCard();
            console.log(answer);
            $scope.card = {
                answer: answer,
                form: forms[answerKey].name,
                infinitif: verb.infinitif,
                pronoun: getPronoun(forms[answerKey].pronoun, answer),
                tense: tense.tense,
            };
        };
        resetAnswerColor();
        $scope.checkAnswer = function (answer) {
            if(answer === ''){
                resetAnswerColor();
            }
        };
        $scope.answer = '';
        $scope.history = [];
        $scope.showingHistory = false;
        $scope.toggleHistory = function (){
            $scope.showingHistory = !$scope.showingHistory;
            $scope.menuOpen = false;
        };
        $scope.showingSkip = false;
        $scope.toggleSkip = function (){
            $scope.showingSkip = !$scope.showingSkip;
            $scope.menuOpen = false;
        };
        $scope.showingCount = false;
        $scope.toggleCount = function (){
            $scope.showingCount = !$scope.showingCount;
            $scope.menuOpen = false;
        };
        $scope.countHistory = function (){
            return $scope.history.length;
        };
        getVerbs($scope.getCard);
        $scope.menuOpen = false;
		$scope.toggleMenu = function () {
			$scope.menuOpen = !$scope.menuOpen;
		};
        $scope.answerIsFocused = true;
        $scope.setIsFocused = function(bool){
            $scope.answerIsFocused = bool;
        }
        $scope.keypress = function (e) {
            var k = e.keyCode || e.keyWhich,
                answer = e.currentTarget.value;
            if(k === 13 && !commands(answer)) {
                if(answerWorks(answer)) {
                    answerAccepted();
                    addHistory();
                    $scope.getCard();
                } else {
                    answerRejected();
                }
            }
        };
    }]);
