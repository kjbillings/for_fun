var app = angular.module("App", ["ui.router"])
    .directive("appLayout", function () {
    return {
        templateUrl: "./src/global/app/app.html"
    };
}).config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("/");
        $stateProvider.state("home", {
            url: "/",
            controller: "HomeCtrl",
            templateUrl: "./src/global/home/home.html"
        }).state("add", {
            url: "/verbs/add",
            controller: "AddCtrl",
            templateUrl: "./src/global/add/add.html"
        });
    }
]);
