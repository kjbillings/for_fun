# README #

This app is designed to be a boilerplate for building a web application.

It requires bit of set up, but hopefully by laying it out in this ReadMe we can make quick work of setting it all up.

# To build on this foundation, you’ll need the following:
* You will need your code editor to have linters for es6 & typescript
* nodeJS
* npm
> ## npm packages to install
> * angular
> * ui-router
> * webpack
> * gulp
> * gulp-sass
> * gulp-autoprefixer
> * gulp-cssnano
> * gulp-jshint
> * gulp-concat
> * gulp-uglify
> * gulp-imagemin
> * gulp-notify
> * gulp-rename
> * gulp-livereload
> * gulp-cache
> * del

# Instructions for starting up the dev environment

### In a new terminal window, run TypeScriptCompiler in the background so you can work in .ts files rather than .js ###
* In a new terminal window
* cd into the repo’s top level folder: “app”
* `tsc index -watch`

### In a new terminal window, run webpack in the background so you can use require.js ###
* In a new terminal window
* cd into the repo’s top level folder: “app”
* `webpack index.js bundle.js --watch`

### In a new terminal window, run gulp in the background so you can use all the gulp tasks and watchers for this project ###
* cd into the repo’s top level folder: “server”
* `gulp`

### In a new terminal window, start the node server so you can see the app in the browser ###
* cd into the repo’s top level folder: “server”
* `node server.js`

—

### You should be good to go once you’ve got all of that running. ###
* Go to http://localhost:3600 to see your app’s UI
* Code in TypeScript & the latest ECMAscript, you don’t have to, but http://www.typescriptlang.org
