var gulp   = require('gulp'),
sass       = require('gulp-sass'),
livereload = require('gulp-livereload'),
sassSrc    = '../app/src/**/*.scss',
cssSrc     = '../app/css';
console.log(sassSrc);
gulp.task('sass', function () {
  return gulp.src(sassSrc)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(cssSrc));
});

gulp.task('watchers', function () {
	livereload.listen(3636 , function(err) {
    if(err) return console.log(err);
  });
  gulp.watch(sassSrc, ['sass']).on('change', livereload.changed);
  // gulp.watch(sassSrc + "/*.scss", ['sass']).on('change', livereload.changed);
  // gulp.watch(sassSrc + "/*/*.scss", ['sass']).on('change', livereload.changed);
  // gulp.watch(sassSrc + "/*/*/*.scss", ['sass']).on('change', livereload.changed);
});

gulp.task('default', function() {
    gulp.start('watchers');
});
