var fs = require('fs');
module.exports = function (res) {
    var arr = [],
        len = 0,
        readFiles = function (dirname, onFileContent, onError) {
            fs.readdir(dirname, function(err, filenames) {
                if (err) {
                  onError(err);
                  return;
                }
                var len = filenames.length;
                filenames.forEach(function(filename) {
                  fs.readFile(dirname + filename, 'utf-8', function(err, content) {
                     len--;
                    if (err) {
                      onError(err);
                      return;
                    }
                    onFileContent(filename, content);
                  });
                });
            });
        };

    readFiles('./verbs/', function (filename, content) {
        arr.push(JSON.parse(content));
        if(len === 0) {
            res.json({verbs: arr});
        }
    }, function (err) {
        console.log("read files error!");
        console.log(err);
    });
};
