var jsonfile = require('jsonfile');
module.exports = function (payload, res) {
    if(payload.name === ''){
        res.send({error: 'Infinitif required', statusText: "Error", status: 400});
    } else {
        var response = {success: true},
            fileName = payload.name + '.json';
        jsonfile.writeFile('./verbs/' + fileName, payload.verb, function (err) {
          console.error(err);
          response = err;
        });
        return res.json(response);
    }
};
