var frontendPort    = 3600,
backendPort         = 3601,
domain              = 'localhost',
rootUrl             = __dirname.replace("/server", ""),
express 			= require('express'),
frontend 			= express(),
backend				= express(),
bodyParser          = require('body-parser'),
router              = express.Router(),
structure           = require('./structure.json'),
processor           = require('./verbiage/processor.js')
getVerbs            = require('./verbiage/get_verbs.js');
backend.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://' + domain + ':' + frontendPort);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
backend.use(bodyParser.urlencoded({ extended: true }));
backend.use(bodyParser.json());
router.post('/verbs', function(req, res) {
    console.log('--------------------------------------------');
    console.log('JSON Received:');
    console.log(req.body);
    processor(req.body, res);
});
router.get('/structure', function (req, res) {
    console.log('--------------------------------------------');
    console.log('Structure Requested');
    res.json(structure);
});
router.get('/verbs', function (req, res) {
    console.log('--------------------------------------------');
    console.log('Verbs Requested');
    getVerbs(res);
});
backend.use('/api', router);


backend.listen(backendPort, domain);
console.log('Backend listening on ' + domain + ':' + backendPort);

frontend.use(express.static(rootUrl + '/app'));
frontend.get('/*', function(req, res){
  res.sendFile(rootUrl + '/app/index.html');
});
frontend.listen(frontendPort, domain);
console.log('Frontend listening on ' + domain + ':' + frontendPort);
